const products = require('./products')

module.exports = (app) => {
  app.use('/api/v1/products', products)
}