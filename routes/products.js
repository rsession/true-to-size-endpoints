const dotenv = require('dotenv')
dotenv.config()

const Router = require('express-promise-router')
const { check, validationResult } = require('express-validator/check')
const debug = require('debug')('http');
const db = require('../db')
const logger = require('../logger')
const router = new Router()

module.exports = router

// New entry for true-to-size submission. id is the product (shoe) id
router.post('/:id/tts', [
	check('id').exists().isInt({min: 1}),
	check('tts_rating').exists().isInt({min: process.env.TTS_MIN, max: process.env.TTS_MAX}),
	check('user_id').exists().isInt({min: 1}),
], async (req, res) => {
	const errors = validationResult(req)
	
	if(!errors.isEmpty()) {
		return res.status(422).json({ errors: errors.array() })
	}

	try {
		const product = await db.query('SELECT id FROM products WHERE id = $1', [req.params.id])

		if(product.rows.length === 1) {
			const result = await db.query('INSERT INTO product_size_ratings(user_id, product_id, tts_rating) VALUES($1, $2, $3) RETURNING *', [req.body.user_id, req.params.id, req.body.tts_rating])

			return res.status(200).json({
				success: true,
			})
		}

		throw new Error(`Unable to find product with id: ${req.params.id}`)
	} catch (err) {
		logger.log('error', err.message)

		// Create a more user-friendly message if a user tries to submit true-to-size information more than once for a particular shoe
    	let message = err.message
    	if(message.indexOf('duplicate key') >= 0) {
    		message = 'You have already submitted a response for this product.'
    	}

		return res.status(500).json({
        	success: false,
        	message,
        })
	}
})

// Retrieve the average rating of true-to-size submissions based on product (shoe) id
router.get('/:id/tts', [
	check('id').exists().isInt({min: 1}),
], async (req, res) => {
	const errors = validationResult(req)
	
	if(!errors.isEmpty()) {
		return res.status(422).json({ errors: errors.array() })
	}

	try {
		const rows = await db.query('SELECT AVG(tts_rating) as rating FROM product_size_ratings WHERE product_id = $1', [req.params.id])

		let status = 200
		let response = {
			success: true,
			tts: rows.rows[0].rating,
		}

		if(rows.rows[0].rating === null) {
			response = rows.rows[0].rating
			status = 404
		}

		return res.status(status).json(response)
	} catch (err) {
		logger.log('error', err.message)

		return res.status(500).json({
        	success: false,
        	message: err.message,
        })
	}
})