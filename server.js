const express = require('express')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const mountRoutes = require('./routes')
const app = express()

app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(helmet())
app.disable('x-powered-by')

mountRoutes(app)

app.listen(process.env.SERVER_PORT)