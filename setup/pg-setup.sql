DROP TABLE IF EXISTS products; 
DROP TABLE IF EXISTS product_size_ratings; 

CREATE TABLE IF NOT EXISTS products(
	id SERIAL PRIMARY KEY, 
	name VARCHAR NOT NULL, 
	description TEXT, 
	ticker VARCHAR NOT NULL, 
	slug TEXT NOT NULL, 
	retail_price FLOAT(2) NOT NULL, 
	release_date DATE NOT NULL, 
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(), 
	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(), 
	deleted_at TIMESTAMPTZ); 

CREATE TABLE IF NOT EXISTS product_size_ratings(
	user_id INTEGER, 
	product_id INTEGER, 
	tts_rating SMALLINT NOT NULL, 
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(), 
	PRIMARY KEY (user_id, product_id)); 

INSERT INTO products(name, description, ticker, slug, retail_price, release_date) 
VALUES('adidas Yeezy Boost 350 V2 Sesame', 'We don&apos;t know if Kanye loves Sesame seeds but we do know that he&apos;s released the adidas Yeezy Boost 350 V2 Sesame. This major silhouette comes with a sesame upper, sesame midsole, and a gum sole. These sneakers released in November 2018 and retailed for $220. While getting your grub on stay fresh and cop these on StockX today.', 'YZY350V2-SESAME', 'adidas-yeezy-boost-350-v2-sesame', 220, '2018-11-23');