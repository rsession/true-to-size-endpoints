
## Installation

```
mv .env.example .env
docker-compose up
```

Application runs on port 3000.

## Usage

### Create New True-To-Size (TTS) for Product
Example Request:
**POST http://local_network_ip:3000/api/v1/products/(product_id)/tts**

Example Request Body:
```
{
	"tts_rating": integer[1-5],
	"user_id": integer > 0
}
```

Example Request Payload:
```
{
	"success": true,
}
```

Example Request Error Payload:
```
{
	"success": false,
	"message": "Server Error"
}
```

### Retrieve Average True-To-Size (TTS) for Product
Example Request:
**GET http://local_ip:3000/api/v1/products/(product_id)/tts**

Example Request Payload:
```
{
	"success": true,
	"tts_rating": 3.48558322
}
```

Example Request Error Payload:
```
{
	"success": false,
	"message": "Server Error"
}
```

## Final Notes

### Improvements

* Add test suite for endpoints
