const winston = require('winston')
const debug = require('debug')('log')

// Setup Winston logging
const logger = winston.createLogger({
	level: winston.config.syslog.levels,
  	format: winston.format.json(),
  	transports: [
  		new winston.transports.Console({
    		format: winston.format.simple()
  		}),
  	]
})

if(process.env.NODE_ENV === 'production') {
	logger.add(new winston.transports.File({ 
    filename: 'server.log', 
    level: 'error',
    format: winston.format.combine(
      winston.format.timestamp(new Date().toUTCString()),
      winston.format.json()
    ),
  }))
}

module.exports = {
  log: (level, err) => {
  	logger.log(level, err)
  	
  	debug(err)
  },
}