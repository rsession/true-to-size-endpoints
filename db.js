const { Pool } = require('pg')
const debug = require('debug')('db')

const pool = new Pool({
	user: process.env.DATABASE_USER,
	host: process.env.DATABASE_HOST,
	database: process.env.DATABASE_NAME,
	password: process.env.DATABASE_PASSWORD,
	port: process.env.DATABASE_PORT,
})

pool.on('error', (err, client) => {
	debug('error', 'Unexpected error on idle client', err)
  	process.exit(-1)
})

module.exports = {
  query: (text, params) => pool.query(text, params),
  end: () => pool.end()
}